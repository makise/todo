#!/bin/bash

args=("$@")

if [[ ${1} == "prod" ]]; then
    echo "prod build"
    export RUSTFLAGS=""
    ARGS="--release"
    if [[ ${2} ]]; then
        cargo ${2} $ARGS ${args[@]:2}
    else
        echo "defaulting to build"
        cargo build $ARGS 
    fi
else
    echo "dev build"
    export RUSTFLAGS="-Zinstrument-coverage -Zmacro-backtrace"
    ARGS=""
    if [[ ${1} ]]; then
        cargo ${1} $ARGS ${args[@]:1}
    else
        echo "defaulting to build+tests"
        cargo test
        grcov . --binary-path ./target/debug -s . -t html --branch --ignore-not-existing -o ./test_coverage/
    fi
fi