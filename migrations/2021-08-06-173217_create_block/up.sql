-- Your SQL goes here
CREATE TABLE IF NOT EXISTS blocks (
    id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    user_id UUID NOT NULL,
    block_type VARCHAR(255) NOT NULL,
    props JSON NOT NULL,
    children TEXT[],
    created_at TIMESTAMP DEFAULT NOW() NOT NULL,
    updated_at TIMESTAMP DEFAULT NOW() NOT NULL
);

SELECT diesel_manage_updated_at('blocks');