use axum::{prelude::*, routing::BoxRoute};

pub mod block;
pub mod discord;
pub mod user;

async fn hello() -> &'static str {
    "Hi"
}

pub fn get_routes() -> BoxRoute<Body> {
    route("/api/hello", any(hello))
        .nest("/api/auth", discord::get_routes())
        .boxed()
}
