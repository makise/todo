use crate::diesel::{prelude::*, QueryDsl};
use crate::models::*;
use crate::schema::*;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel_tracing::pg::InstrumentedPgConnection;
use std::error::Error;
use tokio_diesel::*;
use uuid::Uuid;

pub async fn create_block(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    block: InsertableBlock,
) -> Result<Block, Box<dyn Error>> {
    let inserted: Block = diesel::insert_into(blocks::table)
        .values(block)
        .get_result_async(pool)
        .await?;
    Ok(inserted)
}

pub async fn update_block(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    block: Block,
) -> Result<Block, Box<dyn Error>> {
    use crate::schema::blocks::dsl::*;
    let result = diesel::update(blocks.filter(id.eq(block.id)))
        .set((
            block_type.eq(block.block_type),
            children.eq(block.children),
            props.eq(block.props),
            user_id.eq(block.user_id),
        ))
        .get_result_async(pool)
        .await?;
    Ok(result)
}

pub async fn find_block_by_id(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    block_id: Uuid,
) -> Result<Block, Box<dyn Error>> {
    use crate::schema::blocks::dsl::*;
    let result: Block = blocks.find(block_id).get_result_async(pool).await?;
    log::error!("{:?}", result);
    Ok(result)
}

pub async fn delete_block_by_id(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    block_id: Uuid,
) -> Result<Block, Box<dyn Error>> {
    use crate::schema::blocks::dsl::*;
    let result: Block = diesel::delete(blocks.filter(id.eq(block_id)))
        .get_result_async(pool)
        .await?;
    Ok(result)
}
