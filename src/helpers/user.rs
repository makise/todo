use crate::diesel::{prelude::*, QueryDsl};
use crate::models::*;
use crate::schema::*;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel_tracing::pg::InstrumentedPgConnection;
use std::error::Error;
use tokio_diesel::*;
use uuid::Uuid;

pub async fn create_user(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    user: InsertableUser,
) -> Result<User, Box<dyn Error>> {
    let inserted: User = diesel::insert_into(users::table)
        .values(user)
        .get_result_async(pool)
        .await?;
    Ok(inserted)
}

pub async fn update_user(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    user: User,
) -> Result<User, Box<dyn Error>> {
    use crate::schema::users::dsl::*;
    let result = diesel::update(users.filter(id.eq(user.id)))
        .set((discord_id.eq(user.discord_id),))
        .get_result_async(pool)
        .await?;
    Ok(result)
}

pub async fn find_user_by_id(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    user_id: Uuid,
) -> Result<User, Box<dyn Error>> {
    use crate::schema::users::dsl::*;
    let result = users.find(user_id).get_result_async::<User>(pool).await?;
    log::error!("{:?}", result);
    Ok(result)
}

pub async fn delete_user_by_id(
    pool: &Pool<ConnectionManager<InstrumentedPgConnection>>,
    user_id: Uuid,
) -> Result<User, Box<dyn Error>> {
    use crate::schema::users::dsl::*;
    let result = diesel::delete(users.filter(id.eq(user_id)))
        .get_result_async(pool)
        .await?;
    Ok(result)
}
