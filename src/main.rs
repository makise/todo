use axum::prelude::*;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use async_redis_session::RedisSessionStore;
use axum::AddExtensionLayer;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel_tracing::pg::InstrumentedPgConnection;
use dotenv::dotenv;
use std::env;
use std::str::FromStr;
use tower_http::trace::TraceLayer;

#[macro_use]
extern crate diesel;
extern crate redis;

mod endpoints;
pub mod helpers;
pub mod migration;
pub mod models;
pub mod schema;
pub mod tests;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let _ = setup_logger();

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set");

    migration::run_migrations(&db_url);
    let manager = ConnectionManager::<InstrumentedPgConnection>::new(&db_url);
    let pool = Pool::builder()
        .build(manager)
        .expect("Could not build connection pool");

    let redis_url = env::var("REDIS_URL").unwrap_or(String::from("redis://localhost"));
    let redis_client =
        redis::Client::open(redis_url.as_str()).expect("Could not create redis client.");

    let root = endpoints::get_routes()
        .layer(TraceLayer::new_for_http())
        .layer(AddExtensionLayer::new(RedisSessionStore::from_client(
            redis_client,
        )))
        .layer(AddExtensionLayer::new(pool));

    let ip = env::var("IP").unwrap_or(String::from("127.0.0.1"));
    let port = env::var("PORT").unwrap_or(String::from("8000"));
    let addr = SocketAddr::from((
        IpAddr::from_str(ip.as_str()).unwrap_or(IpAddr::from(Ipv4Addr::new(127, 0, 0, 1))),
        port.parse().unwrap_or(8000),
    ));

    log::info!("started listening on {:?}", addr);
    hyper::Server::bind(&addr)
        .serve(root.into_make_service())
        .await
        .unwrap();
}

fn setup_logger() -> () {
    let log_level = env::var("LOG_LEVEL").unwrap_or(String::from("INFO"));
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(
            tracing::Level::from_str(log_level.as_str()).unwrap_or(tracing::Level::INFO),
        )
        .finish();

    tracing_log::LogTracer::init().expect("could not init log tracer");

    tracing::subscriber::set_global_default(subscriber).expect("could not set default subscriber");
    // fern::Dispatch::new()
    //     .format(|out, message, record| {
    //         out.finish(format_args!(
    //             "{} <{}> [{}] {}",
    //             chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
    //             record.file().unwrap_or(record.target()),
    //             record.level(),
    //             message
    //         ))
    //     })
    //     .level(log::LevelFilter::from_str(log_level.as_str()).unwrap_or(log::LevelFilter::Info))
    //     .chain(std::io::stdout())
    //     .chain(fern::log_file("latest.log")?)
    //     .apply()?;
    // Ok(())
}
