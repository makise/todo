use diesel::{prelude::*, sql_query};
use diesel_migrations::*;

pub fn reset_database(url: &String) {
    let conn = PgConnection::establish(&url).expect(&format!("Error connecting to {}", url));
    println!("dropping all tables");
    let _ = sql_query("drop table users;").execute(&conn);
    let _ = sql_query("drop table unverified_users;").execute(&conn);
    let _ = sql_query("drop table blocks;").execute(&conn);
    let _ = sql_query("drop table __diesel_schema_migrations;").execute(&conn);
    println!("finished resetting db");
}

pub fn run_migrations(url: &String) {
    println!("running migrations");
    let conn = PgConnection::establish(&url).expect(&format!("Error connecting to {}", url));
    let result = run_pending_migrations(&conn);
    if result.is_err() {
        panic!("could not run migrations: {}", result.err().unwrap());
    }
    println!("finished migrations");
}
