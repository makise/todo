use super::schema::*;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Queryable, Deserialize, Serialize, Debug, Clone, PartialEq)]
pub struct User {
    pub id: Uuid,
    pub discord_id: String,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Insertable, Debug, Clone, PartialEq)]
#[table_name = "users"]
pub struct InsertableUser {
    pub discord_id: String,
}

#[derive(Queryable, Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Block {
    pub id: Uuid,
    pub user_id: Uuid,
    pub block_type: String,
    pub props: serde_json::Value,
    pub children: Option<Vec<String>>,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Insertable, Debug, Clone, PartialEq)]
#[table_name = "blocks"]
pub struct InsertableBlock {
    pub user_id: Uuid,
    pub block_type: String,
    pub props: serde_json::Value,
    pub children: Option<Vec<String>>,
}
