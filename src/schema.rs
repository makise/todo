table! {
    blocks (id) {
        id -> Uuid,
        user_id -> Uuid,
        block_type -> Varchar,
        props -> Json,
        children -> Nullable<Array<Text>>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    migration_versions (id) {
        id -> Int4,
        version -> Varchar,
    }
}

table! {
    unverifiedusers (id) {
        id -> Varchar,
        email -> Nullable<Varchar>,
        discord_only_account -> Nullable<Bool>,
        discord_id -> Nullable<Varchar>,
        password_hash -> Nullable<Varchar>,
        verification_token -> Nullable<Varchar>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    users (id) {
        id -> Uuid,
        discord_id -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(
    blocks,
    migration_versions,
    unverifiedusers,
    users,
);
