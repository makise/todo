pub mod db;

#[cfg(test)]
mod tests {
    // a basic test to ensure that tests are executing.
    #[test]
    fn works() {
        assert_eq!(1, 1);
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn db_tests() {
        let url = String::from("postgres://postgres@localhost/todo_test");
        crate::migration::reset_database(&url);
        crate::migration::run_migrations(&url);
        super::db::run_tests(&url).await;
    }
}
