use crate::helpers::*;
use crate::models::*;
use diesel::r2d2::{ConnectionManager, Pool};

use diesel_tracing::pg::InstrumentedPgConnection;
use std::{error::Error, vec::Vec};
use uuid::Uuid;

async fn get_pool(url: &String) -> Pool<ConnectionManager<InstrumentedPgConnection>> {
    let manager = ConnectionManager::<InstrumentedPgConnection>::new(url);
    Pool::builder()
        .build(manager)
        .expect("Could not build connection pool")
}

async fn user_tests(pool: &Pool<ConnectionManager<InstrumentedPgConnection>>) {
    let user = InsertableUser {
        discord_id: String::from("test"),
    };
    let created_result = user::create_user(pool, user).await;
    if created_result.is_err() {
        panic!("could not create user: {:?}", created_result.err());
    } else {
        assert!(created_result.is_ok());
    }

    let created_user: User = created_result.unwrap();

    let mut new_user = created_user.clone();
    new_user.discord_id = String::from("test2");
    let user_update = new_user.clone();

    let updated_result: Result<User, Box<dyn Error>> = user::update_user(pool, user_update).await;

    if updated_result.is_err() {
        panic!(
            "cound not update user {}: {:?}",
            created_user.id,
            updated_result.err()
        );
    }

    let updated_user = updated_result.unwrap();
    assert_eq!(new_user.id, updated_user.id);
    assert_eq!(new_user.discord_id, updated_user.discord_id);

    let get_result = user::find_user_by_id(pool, created_user.id).await;
    if get_result.is_err() {
        panic!(
            "could not find previously created user {}: {:?}",
            created_user.id,
            get_result.err()
        );
    } else {
        assert_eq!(updated_user, get_result.unwrap());
    }

    let delete_result = user::delete_user_by_id(pool, created_user.id).await;
    if delete_result.is_err() {
        panic!(
            "could not delete user {}: {:?}",
            created_user.id,
            delete_result.err()
        );
    }
}

async fn block_tests(pool: &Pool<ConnectionManager<InstrumentedPgConnection>>) {
    let json = serde_json::from_str("[]");
    let block = InsertableBlock {
        user_id: Uuid::new_v4(),
        block_type: String::from("test"),
        children: Some(Vec::new()),
        props: json.unwrap(),
    };
    let created_result = block::create_block(pool, block).await;
    if created_result.is_err() {
        panic!("could not create block: {:?}", created_result.err());
    } else {
        assert!(created_result.is_ok());
    }

    let created_block: Block = created_result.unwrap();

    let mut new_block = created_block.clone();
    new_block.block_type = String::from("test2");
    let block_update = new_block.clone();

    let updated_result: Result<Block, Box<dyn Error>> =
        block::update_block(pool, block_update).await;

    if updated_result.is_err() {
        panic!(
            "cound not update block {}: {:?}",
            created_block.id,
            updated_result.err()
        );
    }
    let updated_block = updated_result.unwrap();
    assert_eq!(new_block.id, updated_block.id);
    assert_eq!(new_block.block_type, updated_block.block_type);
    assert_eq!(new_block.user_id, updated_block.user_id);
    assert_eq!(new_block.children, updated_block.children);
    assert_eq!(new_block.props, updated_block.props);

    let get_result = block::find_block_by_id(pool, created_block.id).await;
    if get_result.is_err() {
        panic!(
            "could not find previously created block {}: {:?}",
            created_block.id,
            get_result.err()
        );
    } else {
        assert_eq!(updated_block, get_result.unwrap());
    }

    let delete_result = block::delete_block_by_id(pool, created_block.id).await;
    if delete_result.is_err() {
        panic!(
            "could not delete block {}: {:?}",
            created_block.id,
            delete_result.err()
        );
    }
}

pub async fn run_tests(url: &String) {
    let pool = get_pool(url).await;
    user_tests(&pool).await;
    block_tests(&pool).await;
}
